"use client";

import { Product } from "@/components/Product"
import React from "react"

const data = [
  { category: "Fruits", price: "$1", stocked: true, name: "Apple", imgurl: "https://media.istockphoto.com/id/184276818/photo/red-apple.jpg?s=612x612&w=0&k=20&c=NvO-bLsG0DJ_7Ii8SSVoKLurzjmV0Qi4eGfn6nW3l5w=" },
  { category: "Fruits", price: "$1", stocked: true, name: "Dragonfruit", imgurl: "https://i0.wp.com/aniloka.com/wp-content/uploads/2023/03/what-does-dragon-fruit-taste-like.jpg?ssl=1" },
  { category: "Fruits", price: "$2", stocked: false, name: "Passionfruit", imgurl: "https://slyce-product.s3.ap-south-1.amazonaws.com/EXOTIC%20%26%20SEASONAL%20FRUITS_null_PASSION%20FRUIT%20YELLOW%20-%20SWEET_image_2022-11-11T12%3A16%3A29.297.jpg" },
  { category: "Vegetables", price: "$2", stocked: true, name: "Spinach", imgurl: "https://cdn.britannica.com/30/82530-050-79911DD4/Spinach-leaves-vitamins-source-person.jpg" },
  { category: "Vegetables", price: "$4", stocked: false, name: "Pumpkin", imgurl: "https://images.immediate.co.uk/production/volatile/sites/30/2020/02/pumpkin-3f3d894.jpg?quality=90&resize=556,505" },
  { category: "Vegetables", price: "$1", stocked: true, name: "Peas", imgurl: "https://www.pcrm.org/sites/default/files/2022-04/Peas.jpg" }
]

export default function ReactPage() {

  const [searchValue, setSearchValue] = React.useState("")

  const [apiData, setApiData] = React.useState([]);
  const [filteredData, setFilteredData] = React.useState([]);

  // Set Database Data
  React.useEffect(() => {
    setApiData(data)
  }, []);

  // Filtered Data
  React.useEffect(() => {
    setFilteredData(apiData.filter((fruit) => fruit.name.toLowerCase().includes(searchValue.toLowerCase())));
  }, [searchValue, apiData]);

  return (
    <main>
      <section className="flex justify-center gap-4 items-center">
        
        <input id="search" placeholder="Search a fruit..." value={searchValue} onChange={e => setSearchValue(e.target.value)} className="border-[1px] border-slate-400 rounded-xl h-[58px] w-[476px] focus:outline-sky-800 pl-3 focus:placeholder:text-[#333333] my-6"/>
      </section>

      <article className="grid grid-cols-1 md:grid-cols-2 xl:grid-cols-3 p-5 gap-12">

        {filteredData.map(({ category, price, stocked, name, imgurl }) => (
          <Product key={name} category={category} price={price} stocked={stocked} name={name} imgurl={imgurl} />
        ))}

      </article>
    </main>
  )
}