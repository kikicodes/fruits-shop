export function Product({category, price, stocked, name, imgurl}) {
    return (
        <article className="rounded-lg shadow-xl">

            <section className="rounded-t-lg h-96">
                <img className="rounded-t-lg w-full h-full object-cover" src={imgurl}/>
            </section>
            
            <section className={`
                ${name === "Apple" && "bg-red-500"}
                ${name === "Dragonfruit" && "bg-pink-500"}
                ${name === "Passionfruit" && "bg-yellow-500"}
                ${name === "Spinach" && "bg-green-500"}
                ${name === "Pumpkin" && "bg-amber-500"}
                ${name === "Peas" && "bg-green-700"}

                flex flex-col items-center gap-3 rounded-b-lg
            `}>

                <div className="bg-white px-5 py-3 rounded-full m-3 drop-shadow-lg">
                    <p className="text-3xl font-bold">{name}</p>
                </div>

                <div>
                    <p className="text-lg text-justify px-5">Lorem ipsum es el texto que se usa habitualmente en diseño gráfico en demostraciones de tipografías o de borradores de diseño para probar el diseño visual antes de insertar el texto final.</p>
                </div>

                <section className="flex justify-around w-3/4 bg-white p-3 rounded-full my-4 drop-shadow-lg">
                    <div >
                        <p>{category}</p>
                    </div>
                    
                    <div>
                        <p>{price}</p>
                    </div>
                    
                    <div>
                        <p>{stocked ? "Stock" : "No Stock"}</p>
                    </div>
                </section>    
            </section>
            
        </article>
    )
}

/*const Prod = () => {

}*/